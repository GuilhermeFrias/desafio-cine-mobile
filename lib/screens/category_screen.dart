import 'package:desafio_cine_mobile/widgets/movies_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:desafio_cine_mobile/models/movies.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';

class CategoryScreen extends StatelessWidget {
  final List<Movies> listMovies;
  CategoryScreen({this.listMovies});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: CupertinoColors.systemRed,
        middle: Text('Movies'),
      ),
      child: ListView.builder(
        itemBuilder: (context, index) {
          return MoviesTile(movie: listMovies[index]);
        },
        itemCount: listMovies.length,
      ),
    );
  }
}
