import 'dart:ui';

import 'package:desafio_cine_mobile/screens/directors_screen.dart';
import 'package:desafio_cine_mobile/screens/movies_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({@required this.welcome});
  final welcome;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    welcome = widget.welcome.toString();
  }

  String welcome;

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Constants.kNavigatorBarColor,
        middle: Text('CineFlix'),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Text(
                welcome,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 80.0,
                ),
              ),
            ),
          ),
          Expanded(
            child: CupertinoButton(
              color: Constants.kBackgroundColor,
              onPressed: () {
                showCupertinoModalPopup<void>(
                  context: context,
                  builder: (BuildContext context) => CupertinoActionSheet(
                    title: const Text('Cineflix'),
                    message: const Text('Select what you\'re looking for'),
                    actions: <CupertinoActionSheetAction>[
                      CupertinoActionSheetAction(
                        child: const Text('Directors'),
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.push(context,
                              CupertinoPageRoute(builder: (context) {
                            return DirectorsScreen();
                          }));
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: const Text('Movies'),
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.push(context,
                              CupertinoPageRoute(builder: (context) {
                            return MoviesScreen();
                          }));
                        },
                      )
                    ],
                    cancelButton: CupertinoActionSheetAction(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                );
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.film,
                    size: 100.0,
                    color: CupertinoColors.systemRed,
                  ),
                  const Text(
                    'Click Here To find all about your favorite movie or director!',
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
