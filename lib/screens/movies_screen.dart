import 'package:desafio_cine_mobile/models/categories.dart';
import 'package:desafio_cine_mobile/models/movies.dart';
import 'package:desafio_cine_mobile/repository/categories_repository.dart';
import 'package:desafio_cine_mobile/repository/movies_repository.dart';
import 'package:desafio_cine_mobile/screens/category_screen.dart';
import 'package:desafio_cine_mobile/widgets/movies_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';
import 'package:flutter/material.dart';

class MoviesScreen extends StatefulWidget {
  @override
  _MoviesScreenState createState() => _MoviesScreenState();
}

class _MoviesScreenState extends State<MoviesScreen> {
  List<Movies> listMovies = [];
  bool isLoading = true;
  List<Categories> listCategories = [];
  List<Widget> listAction = [];

  void initMoviesScreen() async {
    setState(() {
      isLoading = true;
    });
    listMovies = await MoviesRepository().getRequest();
    listCategories = await CategoriesRepository().getRequest();
    setState(() {
      isLoading = false;
    });
    for (int i = 0; i < listCategories.length; i++) {
      for (int j = 0; j < listCategories.length; j++) {
        if (i != j) {
          if (listCategories[i]
                  .name
                  .toUpperCase()
                  .compareTo(listCategories[j].name.toUpperCase()) ==
              0) {
            listCategories.removeAt(i);
          }
        }
      }
    }
    for (int index = 0; index < listCategories.length; index++) {
      listAction.add(
        CupertinoActionSheetAction(
          onPressed: () {
            List<Movies> categoryMovies = [];
            for (int i = 0; i < listMovies.length; i++) {
              if (listMovies[i]
                      .categoryId
                      .compareTo(listCategories[index].id) ==
                  0) {
                categoryMovies.add(listMovies[i]);
              }
            }
            Navigator.pop(context);
            Navigator.push(context, CupertinoPageRoute(builder: (context) {
              return CategoryScreen(
                listMovies: categoryMovies,
              );
            }));
          },
          child: Text(listCategories[index].name.toUpperCase()),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    initMoviesScreen();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: CupertinoColors.systemRed,
        middle: Text('Movies'),
        trailing: CupertinoButton(
          onPressed: () {
            showCupertinoModalPopup<void>(
              context: context,
              builder: (BuildContext context) => CupertinoActionSheet(
                title: const Text('Filter'),
                message: const Text('Filter by category'),
                actions: listAction,
                cancelButton: CupertinoActionSheetAction(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            );
          },
          child: Icon(
            CupertinoIcons.bars,
            color: CupertinoColors.white,
          ),
        ),
      ),
      child: isLoading
          ? Constants.kLoading
          : ListView.builder(
              itemBuilder: (context, index) {
                return MoviesTile(
                  movie: listMovies[index],
                );
              },
              itemCount: listMovies.length,
            ),
    );
  }
}
