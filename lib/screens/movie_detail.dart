import 'package:flutter/cupertino.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';
import 'package:desafio_cine_mobile/models/movies.dart';

class MovieDetailScreen extends StatelessWidget {
  final Movies movie;
  MovieDetailScreen({this.movie});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Constants.kNavigatorBarColor,
        middle: Text(movie.name),
      ),
      child: Column(
        children: [
          Image.network(movie.image, errorBuilder:
              (BuildContext context, Object exception, StackTrace stackTrace) {
            return Icon(
              CupertinoIcons.film,
              size: 50.0,
            );
          }),
          SizedBox(
            height: 15.0,
            width: 600,
          ),
          Text(
            'Description: ',
            style: Constants.kTextStyleMovie,
          ),
          Text(
            'Year: ${movie.year}',
            style: Constants.kTextStyleMovie,
          ),
          Text(
            'Synopsis: ----',
            style: Constants.kTextStyleMovie,
          ),
        ],
      ),
    );
  }
}
