import 'package:flutter/cupertino.dart';
import 'package:desafio_cine_mobile/models/directors.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';

class DirectorDetail extends StatelessWidget {
  final Directors director;
  DirectorDetail({this.director});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Constants.kNavigatorBarColor,
        middle: Text(director.name),
      ),
      child: Column(
        children: [
          Image.network(director.image, errorBuilder:
              (BuildContext context, Object exception, StackTrace stackTrace) {
            return Icon(
              CupertinoIcons.person_alt,
              size: 100.0,
            );
          }),
          SizedBox(
            height: 15.0,
            width: 600,
          ),
          Text(
            'Description: ---- ----',
            style: Constants.kTextStyleMovie,
          )
        ],
      ),
    );
  }
}
