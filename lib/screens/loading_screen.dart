import 'package:desafio_cine_mobile/screens/home_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
    getHttp();
  }

  Future<void> getHttp() async {
    Response response;
    Dio dio = Dio();
    response = await dio.get('https://cine-flix.herokuapp.com/');
    Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(
      builder: (context) {
        return HomeScreen(
          welcome: response,
        );
      },
    ), (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
        child: CupertinoActivityIndicator(),
      ),
    );
  }
}
