import 'package:desafio_cine_mobile/models/directors.dart';
import 'package:desafio_cine_mobile/repository/directors_repository.dart';
import 'package:desafio_cine_mobile/widgets/directors_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';

class DirectorsScreen extends StatefulWidget {
  @override
  _DirectorsScreenState createState() => _DirectorsScreenState();
}

class _DirectorsScreenState extends State<DirectorsScreen> {
  List<Directors> listDirectors = [];
  bool isLoading = true;

  void initDirectorsScreen() async {
    setState(() {
      isLoading = true;
    });
    listDirectors = await DirectorsRepository().getRequest();
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    initDirectorsScreen();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Constants.kBackgroundColor,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: CupertinoColors.systemRed,
        middle: Text('Directors'),
      ),
      child: isLoading
          ? Constants.kLoading
          : ListView.builder(
              itemBuilder: (context, index) {
                return DirectorsTile(
                  director: listDirectors[index],
                );
              },
              itemCount: listDirectors.length,
            ),
    );
  }
}
