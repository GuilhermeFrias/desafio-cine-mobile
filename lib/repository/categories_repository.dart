import 'package:desafio_cine_mobile/models/categories.dart';
import 'package:dio/dio.dart';

class CategoriesRepository {
  Future getRequest() async {
    Response response;
    Dio dio = Dio();
    response = await dio.get('https://cine-flix.herokuapp.com/categories');
    List<Categories> listCategories = (response.data as List)
        .map((item) => Categories.fromJson(item))
        .toList();
    return listCategories;
  }
}
