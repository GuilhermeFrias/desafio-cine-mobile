import 'package:desafio_cine_mobile/models/movies.dart';
import 'package:dio/dio.dart';

class MoviesRepository {
  Future getRequest() async {
    Response response;
    Dio dio = Dio();
    response = await dio.get('https://cine-flix.herokuapp.com/movies');
    List<Movies> listMovies =
        (response.data as List).map((item) => Movies.fromJson(item)).toList();
    return listMovies;
  }
}
