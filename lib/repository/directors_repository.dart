import 'package:desafio_cine_mobile/models/directors.dart';
import 'package:dio/dio.dart';

class DirectorsRepository {
  Future getRequest() async {
    Response response;
    Dio dio = Dio();
    response = await dio.get('https://cine-flix.herokuapp.com/directors');
    List<Directors> listDirectors = (response.data as List)
        .map((item) => Directors.fromJson(item))
        .toList();
    return listDirectors;
  }
}
