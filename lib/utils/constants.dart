import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Constants {
  static const kLoading = Center(
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
    ),
  );
  static const kBackgroundColor = CupertinoColors.darkBackgroundGray;

  static const kNavigatorBarColor = Colors.red;
  static const kTextStyleMovie = TextStyle(
    fontSize: 25.0,
    color: CupertinoColors.white,
  );
}
