import 'package:desafio_cine_mobile/models/directors.dart';
import 'package:desafio_cine_mobile/screens/director_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DirectorsTile extends StatelessWidget {
  final Directors director;
  DirectorsTile({this.director});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          color: Colors.grey,
        ),
        Row(
          children: [
            Container(
              child: Image.network(director.image, errorBuilder:
                  (BuildContext context, Object exception,
                      StackTrace stackTrace) {
                return Icon(
                  CupertinoIcons.person_alt,
                  size: 50.0,
                );
              }),
              width: 200,
              height: 200,
            ),
            Expanded(
              child: Column(
                children: [
                  Text(
                    director.name.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 30,
                      color: CupertinoColors.white,
                    ),
                  ),
                  CupertinoButton(
                      child: Text('More Details'),
                      onPressed: () {
                        return Navigator.push(context,
                            CupertinoPageRoute(builder: (context) {
                          return DirectorDetail(
                            director: director,
                          );
                        }));
                      })
                ],
              ),
            ),
          ],
        ),
        Divider(),
      ],
    );
  }
}
