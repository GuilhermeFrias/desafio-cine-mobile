import 'package:desafio_cine_mobile/screens/movie_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:desafio_cine_mobile/models/movies.dart';
import 'package:desafio_cine_mobile/utils/constants.dart';

class MoviesTile extends StatelessWidget {
  final Movies movie;
  MoviesTile({this.movie});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(),
        Column(
          children: [
            Container(
              color: CupertinoColors.darkBackgroundGray,
              child: Image(
                image: NetworkImage(movie.image),
                height: 300.0,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    'Name: ' + movie.name,
                    style: Constants.kTextStyleMovie,
                  ),
                ],
              ),
            ),
            CupertinoButton(
                child: Text('More Details'),
                onPressed: () {
                  return Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return MovieDetailScreen(
                      movie: movie,
                    );
                  }));
                })
          ],
        ),
        Divider(),
      ],
    );
  }
}
