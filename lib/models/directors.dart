class Directors {
  final String id;
  final String name;
  final String image;

  Directors({this.id, this.image, this.name});

  factory Directors.fromJson(Map<String, dynamic> json) => Directors(
        id: json['id'],
        name: json['name'],
        image: json['image'],
      );
}
