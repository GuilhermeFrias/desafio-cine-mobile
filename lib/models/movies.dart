class Movies {
  final String id;
  final String name;
  final String categoryId;
  final String directorId;
  final int year;
  final String image;

  Movies({
    this.id,
    this.name,
    this.categoryId,
    this.directorId,
    this.image,
    this.year,
  });

  factory Movies.fromJson(Map<String, dynamic> json) {
    return Movies(
      id: json['id'],
      name: json['name'],
      categoryId: json['categoryId'],
      directorId: json['directorId'],
      year: json['year'],
      image: json['image'],
    );
  }
}
